vimrc\_reload\_filetype.vim
===========================

This plugin installs a hook for re-sourcing the `vimrc` file that follows it
with a command to reload filetype detection and filetype plugin loading for the
current buffer, to correct the issue of `vimrc` `:set` commands clobbering
filetype-specific `:setlocal` options.

It also echoes a nice message so you know it's working.

License
-------

Copyright (c) [Tom Ryder][1].  Distributed under the same terms as Vim itself.
See `:help license`.

[1]: https://sanctum.geek.nz/
