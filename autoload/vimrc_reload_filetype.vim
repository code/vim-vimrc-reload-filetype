" Wrapper function reloads .vimrc and filetypes
function! vimrc_reload_filetype#Reload() abort
  source $MYVIMRC
  if exists('#filetypedetect#BufRead')
    doautocmd filetypedetect BufRead
  endif
  redraw
  echomsg 'Reloaded vimrc: '.fnamemodify($MYVIMRC, ':p:~')
endfunction
